﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EasyUISerial.Enums;
using EasyUISerial.Exception;

namespace EasyUISerial
{
    public static class SerialWorkerControls
    {
        private static Task _workerTask;
        private static CancellationTokenSource _cancellationTokenSource;
        private static readonly List<SerialPort> _liveSerialPorts;

        private static bool _isProcessing;
        public static bool IsProcessing => _isProcessing;

        static SerialWorkerControls()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _liveSerialPorts = new List<SerialPort>();
            _workerTask = new Task(() => WorkSerialPorts(_cancellationTokenSource.Token), _cancellationTokenSource.Token);
        }

        public static void StartProcessing()
        {
            if (IsProcessing)
            {
                throw new ProcessingException("The processing task is already started.");
            }

            _isProcessing = true;
            if (_workerTask.Status == TaskStatus.Created)
            {
                _workerTask.Start();
            }
        }

        public static void EndProcessing()
        {
            if (!IsProcessing)
            {
                throw new ProcessingException("The processing task isn't started yet.");
            }

            _cancellationTokenSource.Cancel();

            while (true)
            {
                switch (_workerTask.Status)
                {
                    case TaskStatus.RanToCompletion:
                        break;
                    case TaskStatus.Canceled:
                        break;
                    default:
                        continue;
                }

                break;
            }

            _cancellationTokenSource = new CancellationTokenSource();

            _workerTask = new Task(() => WorkSerialPorts(_cancellationTokenSource.Token), _cancellationTokenSource.Token);

            _isProcessing = false;
        }

        private static void WorkSerialPorts(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                var connections = SerialConnectionFactory.LiveSerialConnections;
                foreach (var connection in connections.Values)
                {
                    while (!connection.CommandQueue.IsEmpty)
                    {
                        connection.CommandQueue.TryDequeue(out var serialCommand);

                        var serialPort = FetchSerialPortConnection(connection);

                        var lineEndingBytes = GetLineEndingBytes(connection);

                        byte[] commandBytes = serialCommand.GetSendBytes();

                        byte[] sendBuffer = new byte[commandBytes.Length + lineEndingBytes.Length];
                        
                        commandBytes.CopyTo(sendBuffer, 0);
                        lineEndingBytes.CopyTo(sendBuffer, commandBytes.Length);

                        serialPort.Write(sendBuffer, 0, sendBuffer.Length);

                        if (serialCommand.ShouldExpectReturnData)
                        {
                            List<byte> resultsBuffer = new List<byte>();
                            while (true)
                            {
                                byte[] readBuffer = new byte[1];
                                serialPort.Read(readBuffer, 0, 1);
                                byte readByte = readBuffer[0];

                                if (readByte == lineEndingBytes[0])
                                {
                                    break;
                                }

                                resultsBuffer.Add(readByte);
                            }
                            serialCommand.SetResultBytes(resultsBuffer.ToArray());
                        }

                        connection.CompletedQueue.Enqueue(serialCommand);
                    }
                }
            }
        }

        private static byte[] GetLineEndingBytes(SerialConnection connection)
        {
            byte[] lineEndingBytes;
            switch (connection.DefaultLineEnding)
            {
                case LineEnding.NoLineEnding:
                    lineEndingBytes = new byte[0];
                    break;
                case LineEnding.NewLine:
                    lineEndingBytes = new byte[] {0xA}; //magic value is utf-8 encoding byte value
                    break;
                case LineEnding.CarriageReturn:
                    lineEndingBytes = new byte[] {0xD}; //magic value is utf-8 encoding byte value
                    break;
                case LineEnding.Both:
                    lineEndingBytes = new byte[] {0xD, 0xA}; //magic value is utf-8 encoding byte value
                    break;
                default:
                    throw new NotImplementedException("Line ending type not implemented.");
            }
            return lineEndingBytes;
        }

        private static SerialPort FetchSerialPortConnection(SerialConnection connection)
        {
            if (_liveSerialPorts.Any(p => p.PortName == connection.PortName))
            {
                var port = _liveSerialPorts.Single(p => p.PortName == connection.PortName);

                if (!port.IsOpen)
                {
                    port.Open();
                }

                return port;
            }
            else
            {
                var newPort = connection.BuildSerialPort();
                _liveSerialPorts.Add(newPort);
                newPort.Open();
                return newPort;
            }
        }
    }
}
