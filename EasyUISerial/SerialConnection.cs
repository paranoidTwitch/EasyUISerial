﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyUISerial.Enums;

namespace EasyUISerial
{
    public class SerialConnection
    {
        
        
        private readonly ConcurrentQueue<SerialCommand> _commandQueue;
        private readonly ConcurrentQueue<SerialCommand> _completedQueue;

        internal ConcurrentQueue<SerialCommand> CommandQueue => _commandQueue;
        internal ConcurrentQueue<SerialCommand> CompletedQueue => _completedQueue;
        public BaudRate DefaultBaudRate { get; }
        public DataBits DefaultDataBits { get; }
        public StopBits DefaultStopBits { get; }
        public Parity DefaultParity { get; }
        public LineEnding DefaultLineEnding { get; }
        public string PortName { get; }

        public SerialConnection(
            string portName,
            BaudRate defaultBaudRate, 
            DataBits defaultDataBits,
            Parity defaultParity,
            StopBits defaultStopBits, 
            LineEnding defaultLineEnding)
        {
            DefaultBaudRate = defaultBaudRate;
            DefaultDataBits = defaultDataBits;
            DefaultStopBits = defaultStopBits;
            DefaultParity = defaultParity;
            DefaultLineEnding = defaultLineEnding;

            PortName = portName;

            _commandQueue = new ConcurrentQueue<SerialCommand>();
            _completedQueue = new ConcurrentQueue<SerialCommand>();
        }

        public Guid SubmitSerialCommand(SerialCommand command)
        {
            _commandQueue.Enqueue(command);
            return command.RequestId;
        }

        public SerialCommand PollForResult(Guid requestId)
        {
            while (true)
            {
                SerialCommand peek;
                _completedQueue.TryPeek(out peek);

                if (peek != null && peek.RequestId == requestId)
                {
                    SerialCommand returnValue;
                    _completedQueue.TryDequeue(out returnValue);
                    return returnValue;
                }
            }
        }

        internal SerialPort BuildSerialPort()
        {
            return new SerialPort(PortName)
            {
                BaudRate = (int)DefaultBaudRate,
                DataBits = (int)DefaultDataBits,
                Parity = DefaultParity,
                StopBits = DefaultStopBits
            };
        }
    }
}
