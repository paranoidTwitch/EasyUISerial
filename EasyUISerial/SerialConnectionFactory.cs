﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using EasyUISerial.Enums;
using EasyUISerial.Exception;

[assembly: InternalsVisibleTo("EasyUISerial.Test")]

namespace EasyUISerial
{
    public class SerialConnectionFactory
    {
        private static readonly Dictionary<string, SerialConnection> _liveSerialConnections;

        public static BaudRate DefaultBaudRate { get; set; }
        public static DataBits DefaultDataBits { get; set; }
        public static Parity DefaultParity { get; set; }
        public static StopBits DefaultStopBits { get; set; }
        public static LineEnding DefaultLineEnding { get; set; }

        static SerialConnectionFactory()
        {
            _liveSerialConnections = new Dictionary<string, SerialConnection>();

            DefaultBaudRate = BaudRate.R009600;
            DefaultDataBits = DataBits.Eight;
            DefaultParity = Parity.None;
            DefaultStopBits = StopBits.One;
            DefaultLineEnding = LineEnding.NewLine;
        }

        internal static Dictionary<string, SerialConnection> LiveSerialConnections => _liveSerialConnections;

        public SerialConnection GetSerialConnection(string portName)
        {
            string[] reportedPorts = SerialPort.GetPortNames();

            if (reportedPorts.All(rp => rp != portName))
            {
                throw new BadPortNameException("The system did not report that port as an active port.");
            }

            if (_liveSerialConnections.ContainsKey(portName))
            {
                return _liveSerialConnections[portName];
            }
            else
            {
                SerialConnection newConnection = new SerialConnection(portName, DefaultBaudRate, DefaultDataBits, DefaultParity, DefaultStopBits, DefaultLineEnding);
                _liveSerialConnections.Add(portName, newConnection);
                return _liveSerialConnections[portName];
            }
        }
    }
}
