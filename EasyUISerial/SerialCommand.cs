﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyUISerial
{
    public class SerialCommand
    {
        public byte[] SendBuffer;
        public byte[] ReceiveBuffer;

        public readonly Guid RequestId;
        internal bool ShouldExpectReturnData { get; }

        public SerialCommand()
        {
            RequestId = Guid.NewGuid();
            ShouldExpectReturnData = true;
        }

        public SerialCommand(bool expectReturnData)
        {
            RequestId = Guid.NewGuid();
            ShouldExpectReturnData = expectReturnData;
        }
        
        internal byte[] GetSendBytes()
        {
            return SendBuffer;
        }

        internal void SetResultBytes(byte[] resultBytes)
        {
            ReceiveBuffer = resultBytes;
        }

        public void SetSendBuffer(byte[] sendBuffer)
        {
            SendBuffer = sendBuffer;
        }

        public void SetSendBuffer(string commandText)
        {
            SendBuffer = Encoding.Default.GetBytes(commandText);
        }

        public void SetSendBuffer(string commandText, Encoding selectedEncoding)
        {
            SendBuffer = selectedEncoding.GetBytes(commandText);
        }

        public byte[] GetReceiveBuffer()
        {
            return ReceiveBuffer;
        }

        public string GetReceiveBufferString()
        {
            return Encoding.Default.GetString(ReceiveBuffer);
        }

        public string GetReceiveBufferString(Encoding encoding)
        {
            return encoding.GetString(ReceiveBuffer);
        }
    }
}
