﻿namespace EasyUISerial.Exception
{
    public class ProcessingException : System.Exception
    {
        public ProcessingException(string message) : base(message) { }
        public ProcessingException(string message, System.Exception innerException) : base(message, innerException) { }
    }
}
