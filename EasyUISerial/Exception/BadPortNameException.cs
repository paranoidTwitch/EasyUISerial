﻿namespace EasyUISerial.Exception
{
    public class BadPortNameException : System.Exception
    {
        public BadPortNameException(string message) : base(message) { }
        public BadPortNameException(string message, System.Exception innerException) : base(message, innerException) { }
    }
}
