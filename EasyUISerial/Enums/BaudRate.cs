﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyUISerial.Enums
{
    public enum BaudRate
    {
        R000300 = 300,
        R000600 = 600,
        R001200 = 1200,
        R002400 = 2400,
        R004800 = 4800,
        R009600 = 9600,
        R014400 = 14400,
        R019200 = 19200,
        R028800 = 28800,
        R038400 = 38400,
        R057600 = 57600,
        R115200 = 115200
    }
}
