#define MAX_BYTES 20

byte inputBytes[MAX_BYTES];

void setup() {
  memset(inputBytes, 0, sizeof(inputBytes));  

  Serial.begin(9600, SERIAL_8N1);
  Serial.flush();
}

void loop() {
  int bytesRead = Serial.readBytesUntil('\n', (char*)inputBytes, MAX_BYTES);
  if(bytesRead > 0)
  {
    for(int i = 0; i < bytesRead; i++)
    {
      Serial.write(inputBytes[i]);
    }
    Serial.print('\n');
    Serial.flush();
  }
}
