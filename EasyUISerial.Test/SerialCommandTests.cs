﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EasyUISerial.Test
{
    [TestClass]
    public class SerialCommandTests
    {
        [TestMethod]
        public void SerialCommand_DefaultConstructor()
        {
            SerialCommand testCommand = new SerialCommand();

            Assert.AreNotEqual(Guid.Empty, testCommand.RequestId);
            Assert.AreEqual(true, testCommand.ShouldExpectReturnData);
        }

        [TestMethod]
        public void SerialCommand_ReturnDataConstructor()
        {
            SerialCommand testCommand = new SerialCommand(true);

            Assert.AreNotEqual(Guid.Empty, testCommand.RequestId);
            Assert.AreEqual(true, testCommand.ShouldExpectReturnData);

            testCommand = new SerialCommand(false);

            Assert.AreNotEqual(Guid.Empty, testCommand.RequestId);
            Assert.AreEqual(false, testCommand.ShouldExpectReturnData);
        }

        [TestMethod]
        public void SerialCommand_SetSendBufferByteArrayOverload()
        {
            SerialCommand testCommand = new SerialCommand();

            byte[] testArray = new byte[]{ 0x1, 0x2, 0x3 };
            testCommand.SetSendBuffer(testArray);
            byte[] roundTripBytes = testCommand.GetSendBytes();

            for(int i = 0; i < testArray.Length; i++)
            {
                Assert.AreEqual(testArray[i], roundTripBytes[i]);
            }

            
        }

        [TestMethod]
        public void SerialCommand_SetSendBufferStringDefaultEncodingOverload()
        {
            SerialCommand testCommand = new SerialCommand();

            string testString = "UnitTest";
            byte[] testArray = Encoding.Default.GetBytes(testString);
            testCommand.SetSendBuffer(testString);
            byte[] roundTripBytes = testCommand.GetSendBytes();

            for (int i = 0; i < testArray.Length; i++)
            {
                Assert.AreEqual(testArray[i], roundTripBytes[i]);
            }
        }

        [TestMethod]
        public void SerialCommand_SetSendBufferStringSuppliedEncodingOverload()
        {
            SerialCommand testCommand = new SerialCommand();

            string testString = "UT";
            byte[] testArray = Encoding.UTF32.GetBytes(testString);
            testCommand.SetSendBuffer(testString, Encoding.UTF32);
            byte[] roundTripBytes = testCommand.GetSendBytes();

            for (int i = 0; i < testArray.Length; i++)
            {
                Assert.AreEqual(testArray[i], roundTripBytes[i]);
            }
        }

        [TestMethod]
        public void SerialCommand_GetRecieveBuffer()
        {
            SerialCommand testCommand = new SerialCommand();
            
            byte[] testArray = new byte[] { 0x1, 0x2, 0x3 };
            testCommand.SetResultBytes(testArray);
            byte[] roundTripBytes = testCommand.GetReceiveBuffer();

            for (int i = 0; i < testArray.Length; i++)
            {
                Assert.AreEqual(testArray[i], roundTripBytes[i]);
            }
        }

        [TestMethod]
        public void SerialCommand_GetRecieveBufferString()
        {
            SerialCommand testCommand = new SerialCommand();

            string testString = "UnitTest";
            byte[] testArray = Encoding.Default.GetBytes(testString);
            testCommand.SetResultBytes(testArray);
            string roundTripString = testCommand.GetReceiveBufferString();

            Assert.AreEqual(testString, roundTripString);
        }

        [TestMethod]
        public void SerialCommand_GetRecieveBufferStringEncodingOverload()
        {
            SerialCommand testCommand = new SerialCommand();

            string testString = "UT";
            byte[] testArray = Encoding.UTF32.GetBytes(testString);
            testCommand.SetResultBytes(testArray);
            string roundTripString = testCommand.GetReceiveBufferString(Encoding.UTF32);

            Assert.AreEqual(testString, roundTripString);
        }
    }
}
