﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyUISerial.Enums;
using EasyUISerial.Exception;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EasyUISerial.Test
{
    [TestClass]
    public class SerialConnectionFactoryTests
    {
        [TestMethod]
        public void SerialConnectionFactory_Constructor()
        {
            SerialConnectionFactory testFactory = new SerialConnectionFactory();

            Assert.AreNotEqual(null, SerialConnectionFactory.LiveSerialConnections);
            Assert.AreEqual(BaudRate.R009600, SerialConnectionFactory.DefaultBaudRate);
            Assert.AreEqual(DataBits.Eight, SerialConnectionFactory.DefaultDataBits);
            Assert.AreEqual(Parity.None, SerialConnectionFactory.DefaultParity);
            Assert.AreEqual(StopBits.One, SerialConnectionFactory.DefaultStopBits);
            Assert.AreEqual(LineEnding.NewLine, SerialConnectionFactory.DefaultLineEnding);

            SerialConnectionFactory.DefaultBaudRate = BaudRate.R004800;
            SerialConnectionFactory.DefaultDataBits = DataBits.Five;
            SerialConnectionFactory.DefaultParity = Parity.Odd;
            SerialConnectionFactory.DefaultStopBits = StopBits.OnePointFive;
            SerialConnectionFactory.DefaultLineEnding = LineEnding.NoLineEnding;

            testFactory = new SerialConnectionFactory();

            Assert.AreNotEqual(null, SerialConnectionFactory.LiveSerialConnections);
            Assert.AreEqual(BaudRate.R004800, SerialConnectionFactory.DefaultBaudRate);
            Assert.AreEqual(DataBits.Five, SerialConnectionFactory.DefaultDataBits);
            Assert.AreEqual(Parity.Odd, SerialConnectionFactory.DefaultParity);
            Assert.AreEqual(StopBits.OnePointFive, SerialConnectionFactory.DefaultStopBits);
            Assert.AreEqual(LineEnding.NoLineEnding, SerialConnectionFactory.DefaultLineEnding);
        }

        [TestMethod]
        public void SerialConnectionFactory_GetSerialConnectionException()
        {
            try
            {
                SerialConnectionFactory testFactory = new SerialConnectionFactory();
                testFactory.GetSerialConnection("FAIL");
            }
            catch (System.Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(BadPortNameException));
            }
        }

        [TestMethod]
        public void SerialConnectionFactory_GetSerialConnection()
        {
            //static resource put into default state
            SerialConnectionFactory.DefaultBaudRate = BaudRate.R009600;
            SerialConnectionFactory.DefaultDataBits = DataBits.Eight;
            SerialConnectionFactory.DefaultParity = Parity.None;
            SerialConnectionFactory.DefaultStopBits = StopBits.One;
            SerialConnectionFactory.DefaultLineEnding = LineEnding.NewLine;

            string goodTestPortName = ConfigurationManager.AppSettings["ArdiunoTestPortName"];
            SerialConnectionFactory testFactory = new SerialConnectionFactory();
            SerialConnection testConnection = null;
            try
            {
                testConnection = testFactory.GetSerialConnection(goodTestPortName);
            }
            catch
            {
                Assert.Fail("GetSerialConnection method should not return an error for this test.");
            }

            Assert.AreNotEqual(null, SerialConnectionFactory.LiveSerialConnections);
            Assert.AreEqual(1, SerialConnectionFactory.LiveSerialConnections.Count);

            Assert.AreNotEqual(null, SerialConnectionFactory.LiveSerialConnections);
            Assert.AreEqual(BaudRate.R009600, SerialConnectionFactory.DefaultBaudRate);
            Assert.AreEqual(DataBits.Eight, SerialConnectionFactory.DefaultDataBits);
            Assert.AreEqual(Parity.None, SerialConnectionFactory.DefaultParity);
            Assert.AreEqual(StopBits.One, SerialConnectionFactory.DefaultStopBits);
            Assert.AreEqual(LineEnding.NewLine, SerialConnectionFactory.DefaultLineEnding);
            
            Assert.AreEqual(goodTestPortName, testConnection.PortName);
            Assert.AreEqual(BaudRate.R009600, testConnection.DefaultBaudRate);
            Assert.AreEqual(DataBits.Eight, testConnection.DefaultDataBits);
            Assert.AreEqual(Parity.None, testConnection.DefaultParity);
            Assert.AreEqual(StopBits.One, testConnection.DefaultStopBits);
            Assert.AreEqual(LineEnding.NewLine, testConnection.DefaultLineEnding);
        }
    }
}
