﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyUISerial.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EasyUISerial.Test
{
    [TestClass]
    public class SerialWorkerControlsTests
    {
        [TestMethod]
        public void SerialWorkerControls_Constructor()
        {
            Assert.AreEqual(false, SerialWorkerControls.IsProcessing);
        }

        [TestMethod, Timeout(1000)]
        public void SerialWorkerControls_StartStop()
        {
            SerialWorkerControls.StartProcessing();
            SerialWorkerControls.EndProcessing();
            SerialWorkerControls.StartProcessing();
            SerialWorkerControls.EndProcessing();
        }

        [TestMethod, Timeout(1000)]
        public void SerialWorkerControls_LiveArduinoTest()
        {
            //static resource put into default state
            SerialConnectionFactory.DefaultBaudRate = BaudRate.R009600;
            SerialConnectionFactory.DefaultDataBits = DataBits.Eight;
            SerialConnectionFactory.DefaultParity = Parity.None;
            SerialConnectionFactory.DefaultStopBits = StopBits.One;
            SerialConnectionFactory.DefaultLineEnding = LineEnding.NewLine;

            string goodTestPortName = ConfigurationManager.AppSettings["ArdiunoTestPortName"];
            SerialConnectionFactory testFactory = new SerialConnectionFactory();
            SerialConnection testConnection = null;
            try
            {
                testConnection = testFactory.GetSerialConnection(goodTestPortName);
            }
            catch
            {
                Assert.Fail("GetSerialConnection method should not return an error for this test.");
            }
            
            SerialWorkerControls.StartProcessing();

            SerialCommand testCommand = new SerialCommand()
            {
                SendBuffer = new byte[] {0x1, 0x2, 0x3}
            };

            var requestId = testConnection.SubmitSerialCommand(testCommand);

            SerialCommand roundTripCommand = testConnection.PollForResult(requestId);

            SerialWorkerControls.EndProcessing();

            for (int i = 0; i < testCommand.SendBuffer.Length; i++)
            {
                Assert.AreEqual(testCommand.SendBuffer[i], roundTripCommand.ReceiveBuffer[i]);
            }
            
        }

        [TestMethod, Timeout(5000)]
        public void SerialWorkerControls_LiveArduinoTestMultiTask()
        {
            //static resource put into default state
            SerialConnectionFactory.DefaultBaudRate = BaudRate.R009600;
            SerialConnectionFactory.DefaultDataBits = DataBits.Eight;
            SerialConnectionFactory.DefaultParity = Parity.None;
            SerialConnectionFactory.DefaultStopBits = StopBits.One;
            SerialConnectionFactory.DefaultLineEnding = LineEnding.NewLine;

            string goodTestPortName = ConfigurationManager.AppSettings["ArdiunoTestPortName"];
            SerialConnectionFactory testFactory = new SerialConnectionFactory();
            SerialConnection testConnection = null;
            try
            {
                testConnection = testFactory.GetSerialConnection(goodTestPortName);
            }
            catch
            {
                Assert.Fail("GetSerialConnection method should not return an error for this test.");
            }

            SerialWorkerControls.StartProcessing();

            List<Task> testTasks = new List<Task>();
            for (int i = 0; i < 200; i++)
            {
                testTasks.Add(
                    Task.Run(() =>
                    {
                        SerialCommand testCommand = new SerialCommand()
                        {
                            SendBuffer = Encoding.Default.GetBytes(i.ToString())
                        };

                        var requestId = testConnection.SubmitSerialCommand(testCommand);

                        SerialCommand roundTripCommand = testConnection.PollForResult(requestId);

                        for (int j = 0; j < testCommand.SendBuffer.Length; j++)
                        {
                            Assert.AreEqual(testCommand.SendBuffer[j], roundTripCommand.ReceiveBuffer[j]);
                        }
                    })
                );
            }

            foreach (var task in testTasks)
            {
                while(task.Status != TaskStatus.RanToCompletion) { }
            }

            SerialWorkerControls.EndProcessing();
        }
    }
}
