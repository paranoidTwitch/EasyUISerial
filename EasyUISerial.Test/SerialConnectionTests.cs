﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyUISerial.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EasyUISerial.Test
{
    [TestClass]
    public class SerialConnectionTests
    {
        [TestMethod]
        public void SerialConnection_Constructor()
        {
            SerialConnection testConnection = new SerialConnection(
                "COM1",
                BaudRate.R009600,
                DataBits.Eight,
                Parity.None,
                StopBits.One,
                LineEnding.NewLine
            );

            Assert.AreEqual("COM1", testConnection.PortName);
            Assert.AreEqual(BaudRate.R009600, testConnection.DefaultBaudRate);
            Assert.AreEqual(DataBits.Eight, testConnection.DefaultDataBits);
            Assert.AreEqual(Parity.None, testConnection.DefaultParity);
            Assert.AreEqual(StopBits.One, testConnection.DefaultStopBits);
            Assert.AreEqual(LineEnding.NewLine, testConnection.DefaultLineEnding);

            Assert.AreNotEqual(null, testConnection.CommandQueue);
            Assert.AreNotEqual(null, testConnection.CompletedQueue);
        }

        [TestMethod]
        public void SerialConnection_SubmitSerialCommand()
        {
            SerialConnection testConnection = new SerialConnection(
                "COM1",
                BaudRate.R009600,
                DataBits.Eight,
                Parity.None,
                StopBits.One,
                LineEnding.NewLine
            );

            SerialCommand testCommand = new SerialCommand();
            testCommand.SetSendBuffer(new byte[] {0x1, 0x2, 0x3});
            testConnection.SubmitSerialCommand(testCommand);

            Assert.AreEqual(testCommand, testConnection.CommandQueue.First());
        }

        [TestMethod]
        public void SerialConnection_PollForResult()
        {
            SerialConnection testConnection = new SerialConnection(
                "COM1",
                BaudRate.R009600,
                DataBits.Eight,
                Parity.None,
                StopBits.One,
                LineEnding.NewLine
            );

            SerialCommand testCommand = new SerialCommand();
            testCommand.SetSendBuffer(new byte[] { 0x1, 0x2, 0x3 });
            testConnection.SubmitSerialCommand(testCommand);

            Assert.AreEqual(testCommand, testConnection.CommandQueue.First());

            SerialCommand moveCommand;
            testConnection.CommandQueue.TryDequeue(out moveCommand);
            testConnection.CompletedQueue.Enqueue(moveCommand);

            SerialCommand roundTripCommand = testConnection.PollForResult(testCommand.RequestId);

            Assert.AreEqual(testCommand, roundTripCommand);
        }

        [TestMethod]
        public void SerialConnection_BuildSerialPort()
        {
            SerialConnection testConnection = new SerialConnection(
                "COM1",
                BaudRate.R009600,
                DataBits.Eight,
                Parity.None,
                StopBits.One,
                LineEnding.NewLine
            );

            SerialPort testPort = testConnection.BuildSerialPort();

            Assert.AreEqual("COM1", testPort.PortName);
            Assert.AreEqual(9600, testPort.BaudRate);
            Assert.AreEqual(8, testPort.DataBits);
            Assert.AreEqual(Parity.None, testPort.Parity);
            Assert.AreEqual(StopBits.One, testPort.StopBits);
        }
    }
}
